#!/bin/bash
##############################################################
## Knowl Bookshelf - INGEST Science                         ##
## Filename: ./legacy/1-maria-ingest-science.sh             ##
## https://gitlab.com/lucidhack/knowl                       ##
## https://creativecommons.org/licenses/by/4.0/             ##
## Author(s): lucidhack                                     ##
##############################################################

# Change to our ingest folder, and purge last/old file
cd ~/knowl/ingest/
rm -f libgen.rar

# Fetch the science database from your favorite mirror
wget -c http://libgen.rs/dbdumps/libgen.rar

# Unpack the database
unrar x libgen.rar
rm -f libgen.rar

# Remove NO_AUTO_CREATE_USER
sed -i 's/,NO_AUTO_CREATE_USER//' libgen.sql

# Recreate a blank database
sudo mysql -e "DROP DATABASE IF EXISTS libgen;"
sudo mysql -e "CREATE DATABASE libgen;"

# Import our databse into MariaDB
sudo mysql libgen < libgen.sql
rm -f libgen.sql

# Next, you should call the json script
# sudo sh ../legacy/2-maria-to-json-science.sh
