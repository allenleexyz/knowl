#!/bin/bash
##############################################################
## Knowl Bookshelf - INGEST fiction                         ##
## Filename: ./legacy/2-maria-to-json-fiction.sh            ##
## https://gitlab.com/lucidhack/knowl                       ##
## https://creativecommons.org/licenses/by/4.0/             ##
## Author(s): lucidhack                                     ##
##############################################################

# PURGE EXISTING FILES
cd /var/tmp
sudo rm -f ~/knowl/ingest/knowl-libgen-fiction-*.json
sudo rm -f /var/tmp/knowl-libgen-fiction-*.json

# GENERATE MANY JSON FILES (100K books at a time)
counter=0
while [ $counter -le 3000000 ]
do
echo $counter
sudo mysql <<ENDSQL
use libgen_ficbooks
DROP TABLE IF EXISTS knowl;
SET connect_work_size = 4000000000;
create table knowl (Description char(255)) engine=CONNECT table_type=JSON file_name='/var/tmp/knowl-libgen-fiction-master-$counter.json'
option_list='Pretty=1,Jmode=0,Base=1' lrecl=128 as
SELECT fiction.ID,
       fiction.MD5 "MD5",
       fiction.Title,
       fiction.Author,
       fiction.Series,
       fiction.Edition,
       fiction.Language,
       fiction.Year,
       fiction.Publisher,
       fiction.Pages,
       fiction.Identifier,
	   fiction.GooglebookID,
       fiction.ASIN,
       fiction.Coverurl "Cover",
       fiction.Extension,
       fiction.Filesize,
       fiction.Library,
       fiction.Issue,
       fiction.Locator,
       fiction.Commentary,
       fiction.Generic,
       fiction.Visible,
       fiction.TimeAdded,
       fiction.TimeLastModified,
       fiction_hashes.crc32 "CRC32",
       fiction_hashes.edonkey "edonkey",
       fiction_hashes.aich "AICH",
       fiction_hashes.sha1 "SHA1",
       fiction_hashes.tth "TTH",
       fiction_hashes.btih "BTIH",
       fiction_hashes.sha256 "SHA256",
       fiction_description.descr "Description",
       fiction_description.TimeLastModified "DescriptionTimeLastModified",
       ipfs_hashes.ipfs_blake2b "IPFS_Blake2b"
       FROM libgen_fiction.fiction LEFT JOIN libgen_fiction.fiction_hashes ON fiction.MD5=fiction_hashes.md5 LEFT JOIN libgen_fiction.ipfs_hashes ON fiction.MD5=ipfs_hashes.md5 LEFT JOIN libgen_fiction.fiction_description ON fiction.MD5=fiction_description.MD5 ORDER BY fiction.ID LIMIT $counter,100000;
DROP TABLE IF EXISTS knowl;
ENDSQL
counter=$(( $counter + 100000 ))
done

# COMBINE ALL OF THE 1K JSON FILES
sudo cat knowl-libgen-fiction-master*.json >> ~/knowl/ingest/knowl-libgen-fiction-master_temp.json
#sudo rm -f knowl-libgen-fiction*.json

# DELETE LINES CONTAINING ONLY "[" or "]",
# AND DELETE LEADING WHITESPACE FROM ALL LINES
cd ~/knowl/ingest
sudo sed -i '/^\[/d;/^\]/d;s/^[ \t]*//' knowl-libgen-fiction-master_temp.json

#DELETE ILLEGAL AND CONTROL CHARACTERS
sudo sed -i $'s/[^[:print:]\t]//g' knowl-libgen-fiction-master_temp.json

#CORRECT UTF8 FORMATTING ERRORS
iconv -f utf-8 -t utf-8 -c knowl-libgen-fiction-master_temp.json > knowl-libgen-fiction-master-v1_1.json

sudo rm -f knowl-libgen-fiction-master_temp.json

# Next, you should call the index json script
# sudo sh ../bare-metal/index-fiction-master-json.sh
